if (process.env.NODE_ENV !== "production") {
  /* Get .env varibales */
  require("dotenv").config();
}

const express = require("express");
const app = express();
const bcrypt = require("bcrypt");
const passport = require("passport");
const flash = require("express-flash");
const session = require("express-session");
const methodOverride = require("method-override");

/* Passport.js */
const initializePassport = require("./passport-config");
initializePassport(
  passport,
  (email) => users.find((user) => user.email === email),
  (id) => users.find((user) => user.id === id)
);

const users = [];

/* Use .ejs */
app.set("view-engine", "ejs");
/* Enable access to form data from request variable inside post methods */
app.use(express.urlencoded({ extended: false }));
app.use(flash());
app.use(
  session({
    secret: process.env.SESSION_SECRET, // make this as random as possible
    resave: false, // Resave session varibles if nothing is changed when changing page
    saveUninitialized: false, // Save empty value, if there is no value
  })
);
/* Use the initialize function from passport-config */
app.use(passport.initialize());
/* Make sessions persistent */
app.use(passport.session());
/* Allow override POST-action with <form action="/route?_method=METHOD_NAME ..." */
app.use(methodOverride("_method"));

/* Routes */
app.get("/", checkAuthenticated, (req, res) => {
  res.render("index.ejs", { name: req.user.name });
});

// Get login view
app.get("/login", checkNotAuthenticated, (req, res) => {
  res.render("login.ejs");
});
// Post login form;
app.post(
  "/login",
  checkNotAuthenticated,
  passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true,
  })
);
// app.post("/login", (req, res) => {
//   res.render("login.ejs");
// });

// Get register view
app.get("/register", checkNotAuthenticated, (req, res) => {
  res.render("register.ejs");
});
// Post register form;
app.post("/register", checkNotAuthenticated, async (req, res) => {
  try {
    const hashedPassword = await bcrypt.hash(req.body.password, 10); // Last param determines rounds of salt to the password (How secure do I want this password)
    users.push({
      id: Date.now().toString(),
      name: req.body.name,
      email: req.body.email,
      password: hashedPassword,
    });

    res.redirect("/login");
  } catch {
    res.redirect("/register");
  }

  console.log(users);
});

/* Logout */
app.delete("/logout", checkAuthenticated, (req, res) => {
  req.logOut();
  res.redirect("/login");
});

/* Methods */

function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect("/login");
  }
}

function checkNotAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect("/");
  }
  next();
}

app.listen(3000);
