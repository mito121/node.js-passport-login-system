## Passport config is set to localhost and using a local array for data storage.

### To run this app:
 - Install dependencies with `npm i`
 - Create new file in root dir called `.env`
 - Declare SESSION_SECRET variable. Example `SESSION_SECRET=secret`
 - Run command `npm run serve`
